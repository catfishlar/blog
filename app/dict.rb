module Dict
  def Dict.new(num_buckets=256)
    a_dict = []
    (0..num_buckets).each {a_dict.push([])}
    return a_dict
  end
  
  def Dict.hash_key(a_dict, key)
    return key.hash % a_dict.length
  end
  
  def Dict.get_bucket(a_dict, key)
    bucket_id = Dict.hash_key(a_dict,key)
    return a_dict[bucket_id]   
  end
  
  def Dict.get_slot(a_dict, key, default = nil)
    bucket = Dict.get_bucket(a_dict, key)
    bucket.each_with_index do |kv,i |
      k,v =kv
      if key == k
        return i, k, v
      end
    end
    return -1, key, default
  end

  def Dict.get(a_dict, key, default=nil)
    i,k,v = Dict.get_slot(a_dict, key, default = default)
    return v
  end

  def Dict.set(a_dict, key, value)
    bucket = Dict.get_bucket(a_dict, key)
    i, k, v = Dict.get_slot(a_dict, key)

    if i < 0 # bucket not found returns -1
      bucket.push([key, value])
    else
      bucket[i] = [key,value]
    end
  end

  def Dict.delete(a_dict, key)
    bucket = Dict.get_bucket(a_dict, key)
    (0..bucket.length).each do |i|
      k,v = bucket[i]
      if key == k
         bucket.delete_at(i)
       break
      end
    end
  end

  def Dict.list(a_dict)
    a_dict.each do |b|
      if b.length > 0
        b.each {|k,v| puts k,v}
      end
    end
  end

end