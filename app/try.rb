everything = "Most Things"
puts "Lets practice #{everything}"
puts 'You\'d need to know \'bout escapes with \\ that do \n newlines and \t tabs.'

poem = <<END
\tThe lovely world
with logic so firmly planted
cannot discern \n the needs of love
nor comprehend passion from intuition
and requires an eplaination
\n\t\twhere there is none.
END
puts "-----------------"
puts poem
puts "------------------"

five = 10 -2 + 3 -6
puts "this should be five: #{five}"

def secret_formula(started)
  jelly_beans = started * 500
  jarz  = jelly_beans / 1000
  crates =  jarz / 100
  return jelly_beans, jarz, crates
end

start_point = 100000
beans, jars, crates = secret_formula(start_point)

puts "with a starting point of: #{start_point}"
puts "We'd have #{beans} beans, #{jars} jars, and #{crates} crates"

the_count = []
(1..5).each {|num| the_count.push(num)}
fruits = ['apples', 'oranges', 'pears', 'apricots']
change = [1, 'pennies',2 ,'dimes', 3, 'quarters']

for bob in the_count
  puts "this is the count #{bob}"
end

fruits.each do |fruit|
  puts"a fruit of type #{fruit}"
end

change.each {|i,j| puts  "#{i} is related to #{j}"}

