module Ex25
  def Ex25.break_words(stuff)
    words = stuff.split(' ')
    return words
  end

  # Sorts the words.
  def Ex25.sort_words(words)
    return words.sort
  end

  # Prints the first word after shifting it off.
  def Ex25.print_first_word(words)
    word = words.shift
    puts word
  end

  # Prints the last word after popping it off.
  def Ex25.print_last_word(words)
    words = words.pop
    puts words
  end

  # takes in a full sentence andreturns the sorted words.
  def Ex25.sort_sentance(sentence)
    words = Ex25.break_words(sentence)
    return Ex25.sort_words(words)
  end

  # Prints the first and last words of the sentance
  def Ex25.print_first_and_last(sentance)
    words = Ex25.break_words(sentance)
    Ex25.print_first_word(words)
    Ex25.print_last_word(words)
  end

  # sorts the words then prints the first and last one.
  def Ex25.print_first_and_last_sorted(sentance)
    words = Ex25.sort_sentance(sentance)
    Ex25.print_first_word(words)
    Ex25.print_last_word(words)
  end

end